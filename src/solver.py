import random


# filters


def black_filter(words, letter):
    """Keep words that don't contain the letter."""
    return [word for word in words if letter.lower() not in word.lower()]


def green_filter(words, letter, position):
    """"Keep words that contain a specific letter at a specific position."""
    words_to_keep = []

    for word in words:
        if word[position].lower() == letter.lower():
            words_to_keep.append(word)
    
    return words_to_keep


# reducer


def reduce_words(words, answer, guess):
    # black filter   
    for i in range(5):
        if guess[i] not in answer:
            words = black_filter(words, guess[i])

    # green filter   
    for i in range(5):
        if guess[i] == answer[i]:
            words = green_filter(words, guess[i], i)

    return words


# game play


def get_answer(list_of_words):
    return (random.choice(list_of_words)).lower()