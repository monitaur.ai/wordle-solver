from src.solver import black_filter, get_answer, green_filter


def test_black_filter():
    words = ["ready", "PILLS"]
    letter = "e"
    new_words = ["PILLS"]

    assert black_filter(words, letter) == new_words


def test_green_filter():
    words = ["ready", "PILLS"]
    letter = "i"
    position = 1
    new_words = ["PILLS"]

    assert green_filter(words, letter, position) == new_words