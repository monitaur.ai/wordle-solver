# Wordle Game with "Assistance"

1. Quick, hacky flavor of [Wordle](https://www.powerlanguage.co.uk/wordle/) with computer "assistance".
1. Limited to 53, 5-letter words.
1. Cheers!

To play:

```sh
$ python main.py
```