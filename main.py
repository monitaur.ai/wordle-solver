from src.solver import get_answer, reduce_words


def main():
    words = [
        "cigar", "rebut", "sissy", "humph", "awake", "blush", "focal", "evade", "naval", "serve", "heath", "dwarf", "model", "karma",
        "stink","grade","quiet","bench","abate","feign","major","death","fresh","crust","stool","colon","abase", "marry", "react",
        "batty", "pride", "floss", "helix", "croak", "staff", "paper", "unfed", "whelp", "trawl", "outdo", "adobe", "crazy", "sower",
        "repay", "digit", "crate", "cluck", "spike", "mimic", "pound", "maxim", "linen", "unmet",
    ]

    answer = get_answer(words)
    round = 1

    while len(words):
        print(f"\n###### WELCOME TO ROUND {round}! ######")
        guess = input("What's your guess? ")
        round = round + 1

        if guess == answer:
            print(f"{guess} is correct!")
            break
        if round > 6:
            break
        else:
            words = reduce_words(words, answer, guess)
            print(f"{len(words)} words remaining!")
            print(words)


if __name__ == "__main__":
    main()